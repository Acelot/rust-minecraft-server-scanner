#![feature(read_buf)]
#![feature(tcp_linger)]
#![feature(tcp_quickack)]
use golden_apple::VarInt;

use indicatif::ParallelProgressIterator;
use rand::thread_rng;
use rayon::prelude::*;
use serde_json::Value;

use rand::seq::SliceRandom;
use std::error::Error;
use std::fs::File;
use std::io::{self, prelude::*, Cursor};
use std::net::{SocketAddr, TcpStream, ToSocketAddrs};
use std::os::linux::net::TcpStreamExt;
use std::path::Path;
use std::sync::{Arc, Mutex};
use std::time::Duration;
// Liveoverflow server?: 162.55.101.24

const MASSCAN_IP_PATH: &str = "./data/masscan";

const VAILD_JSON_DATA_NAME: &str = "vaild_json_received.jsonl";
const RECEIVED_DATA_NAME: &str = "received_data.jsonl";
const FAILED_TO_CONNECT_NAME: &str = "failed_to_connect.jsonl";

const THREADS: usize = 1000; // Todo: Make a argument
const MAX_MINECRAFT_STRING_SIZE: usize = 32767; // Todo: Make a argument
const TIMEOUT: Duration = Duration::from_millis(600); // Todo: Make a argument

// - Note: Use Async instead of rayon.
// When I started this project I did not know about async's advantages.
fn main() -> Result<(), Box<dyn Error + Sync + Send>> {
    let mut ips = extract_ips();
    ips.shuffle(&mut thread_rng());

    // let ips: Vec<&str> = ["127.0.0.1"; 1].to_vec(); // Debug, localhost, should work
    // let ips: Vec<&str> = ["54.224.50.231"; 1].to_vec(); // Debug, Non minecraft server
    // let ips: Vec<&str> = ["213.176.100.102"].to_vec(); // Debug, Odd connection issues resilaince.
    // let ips: Vec<&str> = ["213.176.100.102", "54.224.50.231", "127.0.0.1"].to_vec(); // Debug
                                                                                     

    let len = ips.len();
    let packet = generate_full_status_request_packet()?;

    rayon::ThreadPoolBuilder::new()
        .num_threads(THREADS)
        .panic_handler(|ip| {
            println!("Paniced at IP: {}", ip.downcast_ref::<String>().unwrap());
        })
        .build_global()
        .unwrap();

    let dir = "./data/";
    dbg!(&dir);
    let vaild_json_data_path = dir.to_owned() + VAILD_JSON_DATA_NAME;
    let received_data_path = dir.to_owned() + RECEIVED_DATA_NAME;
    let failed_to_connect_path = dir.to_owned() + FAILED_TO_CONNECT_NAME;

    let vaild_json_data_file = Arc::new(Mutex::new(File::create(vaild_json_data_path).unwrap()));
    let received_data_file = Arc::new(Mutex::new(File::create(received_data_path).unwrap()));
    let failed_to_connect_file =
        Arc::new(Mutex::new(File::create(failed_to_connect_path).unwrap()));
    ips
        .into_par_iter()
        .progress_count(len as u64)
        .for_each(|ip| {
            let ip = ((ip.to_string() + ":25565")
                .to_socket_addrs()
                .unwrap()
                .next())
            .unwrap();
            // FIXME: These unwraps are no good, because I do know know how they interact with rayon.
            //
            // Properly: Masscan ouput parser should output a vector of `SocketAddr`
            // let server = get_status(&packet, ip, TIMEOUT, Some(MAX_MINECRAFT_STRING_SIZE));
            let server = get_status(
                &packet,
                ip,
                TIMEOUT,
                TIMEOUT,
                Some(MAX_MINECRAFT_STRING_SIZE),
            );
            let mut status_template = serde_json::json!({"ip":ip, "status":"" ,"data": {}});
            match server {
                Ok(data) => {
                    match data {
                        // Connected to server and received vaild json.
                        Ok(vaild_json) => {
                            status_template["status"] = "success".into();
                            status_template["data"] = vaild_json;
                            let mut completed_entry = status_template.to_string();
                            completed_entry.retain(|c| c != '\n'); // No newlines in Json L
                            completed_entry += "\n";
                            vaild_json_data_file
                                .lock()
                                .unwrap()
                                .write(completed_entry.as_bytes())
                                .unwrap();
                        }
                        // Connected to server and received data (invaild json).
                        Err(invaild_json) => {
                            status_template["status"] =
                                serde_json::to_value("invaild json received".to_string()).unwrap();
                            status_template["data"] = invaild_json;

                            let mut completed_entry = status_template.to_string();
                            completed_entry.retain(|c| c != '\n');
                            completed_entry += "\n";
                            received_data_file
                                .lock()
                                .unwrap()
                                .write(completed_entry.as_bytes())
                                .unwrap();
                        }
                    }
                }

                // Failed to connected to server
                Err(error) => {
                    status_template["status"] =
                        serde_json::to_value("Failed to connect".to_string()).unwrap();
                    status_template["data"] = serde_json::to_value(error.to_string()).unwrap();
                    let mut completed_entry = status_template.to_string();
                    completed_entry.retain(|c| c != '\n');
                    completed_entry += "\n";
                    failed_to_connect_file
                        .lock()
                        .unwrap()
                        .write(completed_entry.as_bytes())
                        .unwrap();
                }
            }
        });

    Ok(())
}

fn generate_full_status_request_packet() -> Result<Box<Vec<u8>>, Box<dyn Error + Sync + Send>> {
    let mut request_packet = Vec::new();
    request_packet.extend(gen_handshake(None, None)?);
    request_packet.extend(gen_get_status()?);
    let request_packet = Box::new(request_packet);
    Ok(request_packet)
}

fn gen_get_status() -> Result<Vec<u8>, Box<dyn Error + Sync + Send>> {
    let mut payload = Vec::<u8>::new();
    let mut packet = Vec::<u8>::new();

    // PAYLOAD START
    // Get status
    payload.write(&VarInt::from_value(0)?.to_bytes()?)?;
    // PAYLOAD END

    // PACKET START

    // PACKET length
    packet.write(&VarInt::from_value(payload.len() as i32)?.to_bytes()?)?;
    packet.write(&payload)?;
    // PACKET END
    Ok(packet)
}

fn gen_handshake(
    src_ip: Option<String>,
    port: Option<u16>,
) -> Result<Vec<u8>, Box<dyn Error + Sync + Send>> {
    //  OLD get status request: let handschake: [u8; 2] = [0xfe, 0x01];
    // Less bytes to send, but less info.

    // Cleanup payload & packet logic by wrapping it into a function.
    // In order to reduce code duplication.

    // Technically wrong but the Notchian server implementation doesn't use this information.
    let src_ip = src_ip.unwrap_or("127.0.0.1".to_string());
    let port = port.unwrap_or(25565);

    let mut payload = Vec::<u8>::new();
    let mut packet = Vec::<u8>::new();

    // Start PAYLOAD

    // PacketID
    payload.write(&VarInt::from_value(0)?.to_bytes()?)?;

    // Version
    payload.write(&VarInt::from_value(-1)?.to_bytes()?)?;

    // Server Address
    payload.write(&VarInt::from_value(src_ip.len() as i32)?.to_bytes()?)?; // String length
    payload.write(src_ip.as_bytes())?; // String

    // Port
    payload.write(&port.to_be_bytes())?;

    // Status request
    payload.write(&VarInt::from_value(1)?.to_bytes()?)?;

    // END PAYLOAD

    // START Packet
    // Packet length

    packet.write(&VarInt::from_value(payload.len() as i32)?.to_bytes()?)?;
    packet.write(&payload)?;
    // END Packet

    Ok(packet)
}

fn get_status(
    packet: &Box<Vec<u8>>,
    addr: SocketAddr,
    connect_timeout: Duration,
    receive_data_timeout: Duration,
    // If a non-`None` value i it will try to follow the Minecraft protocol to only receive as many bytes as is needed
    // and receiving data until it fills a buffer or times out. (Might be faster if set to `None`)
    // However is set to true some servers might connect in a more stable fashion.
    receive_all_data_size: Option<usize>,
) -> Result<Result<Value, Value>, Box<(dyn Error + Send + Sync)>> {
    let mut connection = TcpStream::connect_timeout(&addr, connect_timeout)?; // Create connection to server

    connection.set_read_timeout(Some(receive_data_timeout))?;
    connection.set_quickack(true)?;
    connection.write_all(&packet)?;

    if let Some(receive_all_data_size) = receive_all_data_size {
        let mut buf: Vec<u8> = vec![0; receive_all_data_size];

        connection.read_exact(&mut buf).err();
        let received_packet = Cursor::new(buf.clone());
        return Ok(read_received_mc_packet(received_packet)?);
    } else {
        return Ok(read_received_mc_packet(connection)?);
    };
}

fn read_received_mc_packet(
    mut connection: impl Read,
) -> Result<Result<Value, Value>, Box<dyn Error + Sync + Send>> {
    let _packet_length = VarInt::from_reader(&mut connection)?.value() as usize;
    let _packet_id = VarInt::from_reader(&mut connection)?.value() as usize;
    let string_size = VarInt::from_reader(&mut connection)?.value() as usize;

    if string_size > MAX_MINECRAFT_STRING_SIZE {
        return Err(Box::<dyn Error + Send + Sync>::from(
            "Invaild packet: String size is too large".to_owned(),
        ));
    };
    let mut buf: Vec<u8> = vec![0; string_size];
    let received_amount = connection.read(&mut buf)?;
    buf.truncate(received_amount);
    let recveived_data = String::from_utf8(buf.clone())?;

    let json: Result<Value, serde_json::Error> = serde_json::from_str(&recveived_data);

    match json {
        Ok(data) => return Ok(Ok(data)), // Received Vaild json
        Err(_error) => {
            return Ok(Err(serde_json::Value::String(
                // Received Invaild json
                String::from_utf8_lossy(&buf).to_string(),
            )));
        }
    }
}

fn lines_from_file<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn extract_ips() -> Vec<String> {
    let contents = lines_from_file(MASSCAN_IP_PATH).unwrap();
    let mut ips = Vec::<_>::new();

    for line in contents {
        let line = line.unwrap();
        let mut line = line.split_whitespace();

        //line.nth(3).and_then(|f|{ips.push(f.to_string()); None::<&str>}); // 1 line is less readable.
        if let Some(ip) = line.nth(3) {
            ips.push(ip.to_string());
        }
    }
    ips
}
