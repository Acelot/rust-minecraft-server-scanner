# MC server dumper V3 
# About

MC server dumper V3 allows you to dump info for minecraft servers.
The reason for its creation was challange by LiveOverFlow.

It dumps all all information to a jsonl (json lines) files.
The files in question:

## Vaild MC servers & server info.


```json
{
  "data": {
    "description": {
      "text": "A Minecraft Server"
    },
    "enforcesSecureChat": true,
    "players": {
      "max": 20,
      "online": 0
    },
    "previewsChat": false,
    "version": {
      "name": "1.19.2",
      "protocol": 760
    }
  },
  "ip": "REDACTED:25565",
  "status": "success"
}

```

## Invaild json with open ports:

```json
{
    "data":
"P/1.1 400 Bad Request\r\nServer: nginx/1.14.0 (Ubuntu)\r\nDate: Tue, 06 Dec 2022 18:59:2",
    "ip":"REDACTED:25565",
    "status":"invaild json received"
}
```

## Failed to connect entries:

```json
{"data":"connection timed out","ip":"REDACTED:25565","status":"Failed to connect"}
{"data":"No route to host (os error 113)","ip":"REDACTED:25565","status":"Failed to connect"}
```

# Work flow

WARNING: YOU WILL GET A BAD IP RATING
SOME SITES WILL START BLOCKING YOU
OR WILL FORCE YOU TO FILL OUT 10 CAPTCHAS
FOR PORT SCANNING USE AT OWN RISK

Step 0: Install the rust toolchain, using `rustup`
- `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`

Step 1: Scan open ports using `masscan`, the scripts folder contains a example script that scans the whole internet.

- `./scripts/start_new_scan.sh (network interface name here)`

Step 2: Run MC DUMPER

- `cargo run --release`

Step 3: View the results

- `cat vaild_json_received.jsonl | jq -C  | less -R`

# Note

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
